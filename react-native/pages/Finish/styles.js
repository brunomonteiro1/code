import styled from 'styled-components/native';
import Button from '../../components/Button';

export const Container = styled.SafeAreaView`
  flex: 1;
`;

export const LabelText = styled.Text`
  color: #fcfef5;
  font-size: 36px;
  font-weight: bold;
  margin-left: 50px;
`;

export const ContainerSlide = styled.View`
  padding: 50px;
`;

export const ContentSlide = styled.View.attrs({
  showsVerticalScrollIndicator: false,
  contentContainerStyle: { padding: 30 },
})`
  margin-bottom: 15px;
  border-radius: 30px;
  background: #fff;
  padding: 40px;
`;

export const TitleSlide = styled.Text`
  font-size: 24px;
  font-weight: bold;
  color: #656565;
`;
export const SubTitleSlide = styled.Text`
  margin-top: 5px;
  font-size: 12px;
  color: #00dffc;
`;
export const TextSlide = styled.Text`
  margin-top: 20px;
  text-align: justify;
`;

export const SubmitButton = styled(Button)`
  margin-top: 30px;
  background-color: #00dffc;
  height: 45px;
  border-radius: 23px;
`;

export const LinkProfile = styled.TouchableOpacity``;
