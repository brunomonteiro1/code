import React from 'react';
import { SafeAreaView, Alert } from 'react-native';
import Lottie from 'lottie-react-native';
import { useSelector } from 'react-redux';
import check from '../../assets/check.json';

export default function Finish({ navigation }) {
  const correct = navigation.getParam('correct');
  const profile = useSelector((state) => state.user.profile);

  function redirect() {
    Alert.alert(
      `Olá ${profile.name}!`,
      `Você respondeu seu DSS com sucesso e acertou ${correct} de 2 perguntas`,
      [{ text: 'VOLTAR', onPress: () => navigation.navigate('Main') }]
    );
  }

  return (
    <SafeAreaView
      style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}
    >
      <Lottie
        resizeMode="contain"
        loop={false}
        autoPlay
        source={check}
        onAnimationFinish={redirect}
      />
    </SafeAreaView>
  );
}

Finish.navigationOptions = () => ({
  title: '',
  headerLeft: () => {},
});
