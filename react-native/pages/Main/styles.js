import styled from 'styled-components/native';
import Button from '../../components/Button';

export const Container = styled.SafeAreaView`
  flex: 1;
  background: #00DFFC;
`;

export const Scroll = styled.ScrollView.attrs({
  showsVerticalScrollIndicator: false,
})`padding-top:5px;`;

export const ContainerSlide = styled.View`
  align-items: center;
  margin-bottom: 15px;
`;

export const LabelText = styled.Text`
  color: #fcfef5;
  font-size: 20px;
  font-weight: bold;
  padding: 0px 50px 15px;
`;

export const ContentSlide = styled.View`
  margin-bottom: 10px;
  border-radius: 20px;
  background: #fff;
  padding: 25px;
  margin: 10px;
`;

export const TitleSlide = styled.Text`
  font-size: 24px;
  font-weight: bold;
  color: #656565;
`;
export const SubTitleSlide = styled.Text`
  margin-top: 5px;
  font-size: 12px;
  color: #00dffc;
`;
export const TextSlide = styled.Text`
  margin-top: 20px;
  text-align: left;
  color: #656565;
  font-weight: bold;
  font-size: 13px;
`;

export const SubmitButton = styled(Button)`
  margin-top: 30px;
  background-color: #00dffc;
  height: 45px;
  border-radius: 23px;
`;

export const LinkProfile = styled.TouchableOpacity``;
