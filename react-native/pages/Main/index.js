import React from 'react';
// import { ScrollView } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import {
  Container,
  LabelText,
  ContentSlide,
  ContainerSlide,
  TitleSlide,
  SubTitleSlide,
  TextSlide,
  SubmitButton,
  LinkProfile,
  Scroll,
} from './styles';

import { Background } from '../../components/Background';
import User from '../../components/User';
import slideItems from '../../assets/slideItems';

// const data = [1, 2, 3, 4, 5];

export default function Main({ navigation }) {

  return (
    <Background>
      <Container>
        <Scroll>
          <LinkProfile onPress={() => navigation.navigate('Profile')}>
            <User />
          </LinkProfile>

          <ContainerSlide>
            <LabelText>Olá, o que deseja fazer agora?</LabelText>
            <Carousel
              data={slideItems}
              layout="tinder"
              layoutCardOffset={15}
              sliderWidth={300}
              itemWidth={300}
              renderItem={({ item }) => (
                <ContentSlide>
                  <TitleSlide>{item.title}</TitleSlide>
                  <SubTitleSlide>{item.subTitle}</SubTitleSlide>
                  <TextSlide>{item.text}</TextSlide>
                  <SubmitButton
                    onPress={() => navigation.navigate(`${item.buttonAction}`)}
                  >
                    {item.buttonText}
                  </SubmitButton>
                </ContentSlide>
              )}
            />
          </ContainerSlide>
        </Scroll>
      </Container>
    </Background>
  );
}

Main.navigationOptions = () => ({
  title: '',
  headerLeft: () => {},
});
