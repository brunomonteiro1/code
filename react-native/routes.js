import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import SignIn from './pages/SignIn';
import SignUp from './pages/SignUp';
import Main from './pages/Main';
import Alert from './pages/Alert';
import AlertSingle from './pages/AlertSingle';
import Profile from './pages/Profile';
import Security from './pages/Security';
import Message from './pages/Message';
import HomeCheck from './pages/HomeCheck';
import Check from './pages/Check';
import Ask1 from './pages/Ask1';
import Ask2 from './pages/Ask2';
import Finish from './pages/Finish';

export default (isSignedIn = false) =>
  createAppContainer(
    createSwitchNavigator(
      {
        Sign: createSwitchNavigator({
          SignIn,
          SignUp,
        }),
        App: createStackNavigator(
          {
            Main,
            AlertSingle,
            Check,
            Ask1,
            Ask2,
            Profile,
            Alert,
            Security,
            Message,
            HomeCheck,
            Finish,
          },
          {
            defaultNavigationOptions: {
              headerTransparent: true,
              headerTintColor: '#FFF',
              headerLeftContainerStyle: {
                marginLeft: 20,
              },
            },
          }
        ),
      },
      {
        initialRouteName: isSignedIn ? 'App' : 'Sign',
      }
    )
  );
