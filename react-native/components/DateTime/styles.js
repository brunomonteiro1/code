import styled from 'styled-components/native';

export const Date = styled.Text`
  text-align: justify;
  color: #fff;
  font-weight: bold;
  margin-bottom: 10px;
`;
