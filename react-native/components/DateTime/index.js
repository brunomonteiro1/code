import React from 'react';
import { format } from 'date-fns';
import { zonedTimeToUtc } from 'date-fns-tz';

import { Date } from './styles';

export default function DateTime({ data }) {
  const date = format(
    zonedTimeToUtc(data, 'America/Recife'),
    'dd/MM/yyyy HH:mm'
  );
  return <Date>{date}</Date>;
}
