import { Request, Response } from 'express';
import { container } from 'tsyringe';
import { classToClass } from 'class-transformer';
import CreateUserService from '../../../services/CreateUserService';
import UpdateUserService from '../../../services/UpdateUserService';
import UpdateAvatarUserService from '../../../services/UpdateUserAvatarService';

export default class UserController {
  public async create(request: Request, response: Response): Promise<Response> {
    const { name, email, password, level, status } = request.body;
    const user_id = request.user.id;
    const ip = request.socket.remoteAddress?.toString();
    const createUser = container.resolve(CreateUserService);

    const user = await createUser.execute({
      name,
      email,
      password,
      level,
      status,
      user_id,
      ip,
    });

    return response.json(classToClass(user));
  }

  public async update(request: Request, response: Response): Promise<Response> {
    const { id, name, email, password, level, status } = request.body;
    const user_id = request.user.id;
    const ip = request.socket.remoteAddress?.toString();
    const updateUser = container.resolve(UpdateUserService);

    const user = await updateUser.execute({
      id,
      name,
      email,
      password,
      level,
      status,
      user_id,
      ip,
    });

    return response.json(classToClass(user));
  }

  public async updateAvatar(
    request: Request,
    response: Response
  ): Promise<Response> {
    const { id } = request.body;
    const user_id = request.user.id;
    const ip = request.socket.remoteAddress?.toString();
    const { filename } = request.file;
    const uploadAvatar = container.resolve(UpdateAvatarUserService);

    const avatar = await uploadAvatar.execute({ id, filename, user_id, ip });

    return response.json(classToClass(avatar));
  }
}
